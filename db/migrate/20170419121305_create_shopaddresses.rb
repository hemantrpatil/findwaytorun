class CreateShopaddresses < ActiveRecord::Migration
  def change
    create_table :shopaddresses do |t|
      t.string :name
      t.string :streetaddress
      t.string :landmark
      t.string :city
      t.string :state
      t.string :pincode
      t.string :mobileno
      t.string :landlineno
      t.string :emailid
      t.string :websiteurl

      t.timestamps null: false
    end
  end
end
