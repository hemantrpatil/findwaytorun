class AddUserIdToShoptimings < ActiveRecord::Migration
  def change
    add_column :shoptimings, :user_id, :integer
  end
end
