class AddUserIdToShopabouts < ActiveRecord::Migration
  def change
    add_column :shopabouts, :user_id, :integer
  end
end
