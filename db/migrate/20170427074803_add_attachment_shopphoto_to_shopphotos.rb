class AddAttachmentShopphotoToShopphotos < ActiveRecord::Migration
  def self.up
    change_table :shopphotos do |t|
      t.attachment :shopphoto
      t.attachment :shopphoto1
        t.attachment :shopphoto2
         t.attachment :shopphoto3
          t.attachment :unicproimg1
           t.attachment :unicproimg2
    end
  end

  def self.down
    remove_attachment :shopphotos, :shopphoto
    remove_attachment :shopabouts, :shopphoto1
    remove_attachment :shopabouts, :shopphoto2
    remove_attachment :shopabouts, :shopphoto3
    remove_attachment :shopabouts, :unicproimg1
    remove_attachment :shopabouts, :unicproimg2
  end
end
