class CreateShopphotos < ActiveRecord::Migration
  def change
    create_table :shopphotos do |t|
      t.string :img_name

      t.timestamps null: false
    end
  end
end
