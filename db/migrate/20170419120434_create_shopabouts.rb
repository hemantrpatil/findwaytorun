class CreateShopabouts < ActiveRecord::Migration
  def change
    create_table :shopabouts do |t|
      t.string :shopname
      t.string :aboutinfo
      t.string :shopmobno
      t.string :shopownername
      t.string :unicproductsforsale

      t.timestamps null: false
    end
  end
end
