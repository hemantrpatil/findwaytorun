class AddUserIdToShopphoto < ActiveRecord::Migration
  def change
    add_column :shopphotos, :user_id, :integer
  end
end
