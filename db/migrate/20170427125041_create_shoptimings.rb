class CreateShoptimings < ActiveRecord::Migration
  def change
    create_table :shoptimings do |t|
      t.string :sunday
      t.string :monday
      t.string :tuesday
      t.string :wednesday
      t.string :thursday
      t.string :friday
      t.string :saturday

      t.timestamps null: false
    end
  end
end
