class AddUserIdToShoplocation < ActiveRecord::Migration
  def change
    add_column :shoplocations, :user_id, :integer
  end
end
