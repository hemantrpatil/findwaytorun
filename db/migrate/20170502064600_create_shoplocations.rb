class CreateShoplocations < ActiveRecord::Migration
  def change
    create_table :shoplocations do |t|
      t.string :location

      t.timestamps null: false
    end
  end
end
