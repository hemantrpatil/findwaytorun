class AddAttachmentOwnerimgToShopabouts < ActiveRecord::Migration
  def self.up
    change_table :shopabouts do |t|
      t.attachment :ownerimg
    end
  end

  def self.down
    remove_attachment :shopabouts, :ownerimg
  end
end
