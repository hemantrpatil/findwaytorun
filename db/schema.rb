# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170502083713) do

  create_table "shopabouts", force: :cascade do |t|
    t.string   "shopname",              limit: 255
    t.string   "aboutinfo",             limit: 255
    t.string   "shopmobno",             limit: 255
    t.string   "shopownername",         limit: 255
    t.string   "unicproductsforsale",   limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "user_id",               limit: 4
    t.string   "ownerimg_file_name",    limit: 255
    t.string   "ownerimg_content_type", limit: 255
    t.integer  "ownerimg_file_size",    limit: 4
    t.datetime "ownerimg_updated_at"
  end

  create_table "shopaddresses", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "streetaddress", limit: 255
    t.string   "landmark",      limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "pincode",       limit: 255
    t.string   "mobileno",      limit: 255
    t.string   "landlineno",    limit: 255
    t.string   "emailid",       limit: 255
    t.string   "websiteurl",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id",       limit: 4
  end

  create_table "shoplocations", force: :cascade do |t|
    t.string   "location",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id",    limit: 4
  end

  create_table "shopphotos", force: :cascade do |t|
    t.string   "img_name",                 limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id",                  limit: 4
    t.string   "shopphoto_file_name",      limit: 255
    t.string   "shopphoto_content_type",   limit: 255
    t.integer  "shopphoto_file_size",      limit: 4
    t.datetime "shopphoto_updated_at"
    t.string   "shopphoto1_file_name",     limit: 255
    t.string   "shopphoto1_content_type",  limit: 255
    t.integer  "shopphoto1_file_size",     limit: 4
    t.datetime "shopphoto1_updated_at"
    t.string   "shopphoto2_file_name",     limit: 255
    t.string   "shopphoto2_content_type",  limit: 255
    t.integer  "shopphoto2_file_size",     limit: 4
    t.datetime "shopphoto2_updated_at"
    t.string   "shopphoto3_file_name",     limit: 255
    t.string   "shopphoto3_content_type",  limit: 255
    t.integer  "shopphoto3_file_size",     limit: 4
    t.datetime "shopphoto3_updated_at"
    t.string   "unicproimg1_file_name",    limit: 255
    t.string   "unicproimg1_content_type", limit: 255
    t.integer  "unicproimg1_file_size",    limit: 4
    t.datetime "unicproimg1_updated_at"
    t.string   "unicproimg2_file_name",    limit: 255
    t.string   "unicproimg2_content_type", limit: 255
    t.integer  "unicproimg2_file_size",    limit: 4
    t.datetime "unicproimg2_updated_at"
  end

  create_table "shoptimings", force: :cascade do |t|
    t.string   "sunday",     limit: 255
    t.string   "monday",     limit: 255
    t.string   "tuesday",    limit: 255
    t.string   "wednesday",  limit: 255
    t.string   "thursday",   limit: 255
    t.string   "friday",     limit: 255
    t.string   "saturday",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id",    limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
