class ShopphotosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_shopphoto, only: [:show, :edit, :update, :destroy]

  # GET /shopphotos
  # GET /shopphotos.json
  def index
    @shopphotos =  current_user.shopphotos  #Shopphoto.all
     if Shopphoto.where(:user_id => current_user.id).present?
         shopphotos_path
     else
          redirect_to new_shopphoto_path
    end
  end

  # GET /shopphotos/1
  # GET /shopphotos/1.json
  def show
  end

  # GET /shopphotos/new
  def new
    @shopphoto = Shopphoto.new
  end

  # GET /shopphotos/1/edit
  def edit
    @shopphotos =  current_user.shopphotos
  end

  # POST /shopphotos
  # POST /shopphotos.json
  def create
    @shopphoto = Shopphoto.new(shopphoto_params)
    @shopphoto.user_id = current_user.id

    respond_to do |format|
      if @shopphoto.save
        format.html { redirect_to shopphotos_path, notice: 'Shopphoto was successfully created.' }
        format.json { render :show, status: :created, location: shopphotos_path }
      else
        format.html { render :new }
        format.json { render json: shopphotos_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shopphotos/1
  # PATCH/PUT /shopphotos/1.json
  def update
    respond_to do |format|
      if @shopphoto.update(shopphoto_params)
        format.html { redirect_to shopphotos_path, notice: 'Shopphoto was successfully updated.' }
        format.json { render :show, status: :ok, location: shopphotos_path }
      else
        format.html { render :edit }
        format.json { render json: shopphotos_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shopphotos/1
  # DELETE /shopphotos/1.json
  def destroy
    @shopphoto.destroy
    respond_to do |format|
      format.html { redirect_to shopphotos_url, notice: 'Shopphoto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shopphoto
      @shopphoto = Shopphoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shopphoto_params
      params.require(:shopphoto).permit(:img_name, :shopphoto,:shopphoto1, :shopphoto2, :shopphoto3, :unicproimg1, :unicproimg2, :user_id)
    end
end
