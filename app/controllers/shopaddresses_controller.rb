class ShopaddressesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_shopaddress, only: [:show, :edit, :update, :destroy]

  # GET /shopaddresses
  # GET /shopaddresses.json
  def index
    @shopaddresses = current_user.shopaddresses
    if Shopaddress.where(:user_id => current_user.id).present?
         shopaddresses_path
     else
          redirect_to new_shopaddress_path
    end    
  end

  # GET /shopaddresses/1
  # GET /shopaddresses/1.json
  def show
  end

  # GET /shopaddresses/new
  def new
    @shopaddress = Shopaddress.new
  end

  # GET /shopaddresses/1/edit
  def edit
  end

  # POST /shopaddresses
  # POST /shopaddresses.json
  def create
    @shopaddress = Shopaddress.new(shopaddress_params)
    @shopaddress.user_id = current_user.id

    respond_to do |format|
      if @shopaddress.save
        format.html { redirect_to shopaddresses_path, notice: 'Shopaddress was successfully created.' }
        format.json { render :show, status: :created, location: shopaddresses_path }
      else
        format.html { render :new }
        format.json { render json: shopaddresses_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shopaddresses/1
  # PATCH/PUT /shopaddresses/1.json
  def update
    respond_to do |format|
      if @shopaddress.update(shopaddress_params)
        format.html { redirect_to shopaddresses_path, notice: 'Shopaddress was successfully updated.' }
        format.json { render :show, status: :ok, location: shopaddresses_path }
      else
        format.html { render :edit }
        format.json { render json: shopaddresses_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shopaddresses/1
  # DELETE /shopaddresses/1.json
  def destroy
    @shopaddress.destroy
    respond_to do |format|
      format.html { redirect_to shopaddresses_url, notice: 'Shopaddress was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shopaddress
      @shopaddress = Shopaddress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shopaddress_params
      params.require(:shopaddress).permit(:name, :streetaddress, :landmark, :city, :state, :pincode, :mobileno, :landlineno, :emailid, :websiteurl, :user_id)
    end
end
