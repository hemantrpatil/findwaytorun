class ShopaboutsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_shopabout, only: [:show, :edit, :update, :destroy]

  # GET /shopabouts
  # GET /shopabouts.json
  def index
    @shopabouts = current_user.shopabouts
    if Shopabout.where(:user_id => current_user.id).present?
         shopabouts_path
     else
          redirect_to new_shopabout_path
    end    
  end

  # GET /shopabouts/1
  # GET /shopabouts/1.json
  def show
  end

  # GET /shopabouts/new
  def new
    @shopabout = Shopabout.new
  end

  # GET /shopabouts/1/edit
  def edit
    @shopabouts = current_user.shopabouts
  end

  # POST /shopabouts
  # POST /shopabouts.json
  def create
    @shopabout = Shopabout.new(shopabout_params)
    @shopabout.user_id = current_user.id

    respond_to do |format|
      if @shopabout.save
        format.html { redirect_to shopabouts_path, notice: 'Shopabout was successfully created.' }
        format.json { render :show, status: :created, location: shopabouts_path }
      else
        format.html { render :new }
        format.json { render json: shopabouts_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shopabouts/1
  # PATCH/PUT /shopabouts/1.json
  def update
    respond_to do |format|
      if @shopabout.update(shopabout_params)
        format.html { redirect_to shopabouts_path, notice: 'Shopabout was successfully updated.' }
        format.json { render :show, status: :ok, location: shopabouts_path }
      else
        format.html { render :edit }
        format.json { render json: shopabouts_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shopabouts/1
  # DELETE /shopabouts/1.json
  def destroy
    @shopabout.destroy
    respond_to do |format|
      format.html { redirect_to shopabouts_url, notice: 'Shopabout was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shopabout
      @shopabout = Shopabout.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shopabout_params
      params.require(:shopabout).permit(:shopname, :aboutinfo, :shopmobno, :shopownername, :unicproductsforsale, :ownerimg,  :user_id)
    end
end
