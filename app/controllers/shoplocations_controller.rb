class ShoplocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_shoplocation, only: [:show, :edit, :update, :destroy]

  # GET /shoplocations
  # GET /shoplocations.json
  def index
    @shoplocations = current_user.shoplocations  #Shoplocation.all
    if Shoplocation.where(:user_id => current_user.id).present?
         shoplocations_path
     else
          redirect_to new_shoplocation_path
    end 
  end

  # GET /shoplocations/1
  # GET /shoplocations/1.json
  def show
  end

  # GET /shoplocations/new
  def new
    @shoplocation = Shoplocation.new
  end

  # GET /shoplocations/1/edit
  def edit
  end

  # POST /shoplocations
  # POST /shoplocations.json
  def create
    @shoplocation = params[:location]
    @shoplocation = Shoplocation.new(shoplocation_params)
    @shoplocation.user_id = current_user.id
    respond_to do |format|
      if @shoplocation.save
        format.html { redirect_to shoplocations_path, notice: 'Shoplocation was successfully created.' }
        format.json { render :show, status: :created, location: shoplocations_path }
      else
        format.html { render :new }
        format.json { render json: shoplocations_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shoplocations/1
  # PATCH/PUT /shoplocations/1.json
  def update
    respond_to do |format|
      if @shoplocation.update(shoplocation_params)
        format.html { redirect_to shoplocations_path, notice: 'Shoplocation was successfully updated.' }
        format.json { render :show, status: :ok, location: shoplocations_path }
      else
        format.html { render :edit }
        format.json { render json: shoplocations_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shoplocations/1
  # DELETE /shoplocations/1.json
  def destroy
    @shoplocation.destroy
    respond_to do |format|
      format.html { redirect_to shoplocations_url, notice: 'Shoplocation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shoplocation
      @shoplocation = Shoplocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shoplocation_params
      params.require(:shoplocation).permit(:location, :user_id)
    end
end
