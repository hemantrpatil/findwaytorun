class ShoptimingsController < ApplicationController
  before_action :set_shoptiming, only: [:show, :edit, :update, :destroy]

  # GET /shoptimings
  # GET /shoptimings.json
  def index
    @shoptimings = current_user.shoptimings #Shoptiming.all
    if Shoptiming.where(:user_id => current_user.id).present?
         shoptimings_path
     else
          redirect_to new_shoptiming_path
    end    
  end

  # GET /shoptimings/1
  # GET /shoptimings/1.json
  def show
  end

  # GET /shoptimings/new
  def new
    @shoptiming = Shoptiming.new
  end

  # GET /shoptimings/1/edit
  def edit
  end

  # POST /shoptimings
  # POST /shoptimings.json
  def create
    @shoptiming = Shoptiming.new(shoptiming_params)
    @shoptiming.user_id = current_user.id

    respond_to do |format|
      if @shoptiming.save
        format.html { redirect_to shoptimings_path, notice: 'Shoptiming was successfully created.' }
        format.json { render :show, status: :created, location: shoptimings_path }
      else
        format.html { render :new }
        format.json { render json: shoptimings_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shoptimings/1
  # PATCH/PUT /shoptimings/1.json
  def update
    respond_to do |format|
      if @shoptiming.update(shoptiming_params)
        format.html { redirect_to shoptimings_path, notice: 'Shoptiming was successfully updated.' }
        format.json { render :show, status: :ok, location: shoptimings_path }
      else
        format.html { render :edit }
        format.json { render json: shoptimings_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shoptimings/1
  # DELETE /shoptimings/1.json
  def destroy
    @shoptiming.destroy
    respond_to do |format|
      format.html { redirect_to shoptimings_url, notice: 'Shoptiming was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shoptiming
      @shoptiming = Shoptiming.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shoptiming_params
      params.require(:shoptiming).permit(:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :user_id)
    end
end
