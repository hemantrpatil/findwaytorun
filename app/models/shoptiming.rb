# == Schema Information
#
# Table name: shoptimings
#
#  id         :integer          not null, primary key
#  sunday     :string(255)
#  monday     :string(255)
#  tuesday    :string(255)
#  wednesday  :string(255)
#  thursday   :string(255)
#  friday     :string(255)
#  saturday   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Shoptiming < ActiveRecord::Base
	belongs_to :user

end
