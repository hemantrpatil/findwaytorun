# == Schema Information
#
# Table name: shopabouts
#
#  id                    :integer          not null, primary key
#  shopname              :string(255)
#  aboutinfo             :string(255)
#  shopmobno             :string(255)
#  shopownername         :string(255)
#  unicproductsforsale   :string(255)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :integer
#  ownerimg_file_name    :string(255)
#  ownerimg_content_type :string(255)
#  ownerimg_file_size    :integer
#  ownerimg_updated_at   :datetime
#

class Shopabout < ActiveRecord::Base
	belongs_to :user



	has_attached_file :ownerimg, styles: { large: "600x600",  medium: "400x250>", thumb: "200x200>" }, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :ownerimg, content_type: /\Aimage\/.*\z/	
end
