# == Schema Information
#
# Table name: shoplocations
#
#  id         :integer          not null, primary key
#  location   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Shoplocation < ActiveRecord::Base
	belongs_to :user
end
