# == Schema Information
#
# Table name: shopphotos
#
#  id                       :integer          not null, primary key
#  img_name                 :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  user_id                  :integer
#  shopphoto_file_name      :string(255)
#  shopphoto_content_type   :string(255)
#  shopphoto_file_size      :integer
#  shopphoto_updated_at     :datetime
#  shopphoto1_file_name     :string(255)
#  shopphoto1_content_type  :string(255)
#  shopphoto1_file_size     :integer
#  shopphoto1_updated_at    :datetime
#  shopphoto2_file_name     :string(255)
#  shopphoto2_content_type  :string(255)
#  shopphoto2_file_size     :integer
#  shopphoto2_updated_at    :datetime
#  shopphoto3_file_name     :string(255)
#  shopphoto3_content_type  :string(255)
#  shopphoto3_file_size     :integer
#  shopphoto3_updated_at    :datetime
#  unicproimg1_file_name    :string(255)
#  unicproimg1_content_type :string(255)
#  unicproimg1_file_size    :integer
#  unicproimg1_updated_at   :datetime
#  unicproimg2_file_name    :string(255)
#  unicproimg2_content_type :string(255)
#  unicproimg2_file_size    :integer
#  unicproimg2_updated_at   :datetime
#

class Shopphoto < ActiveRecord::Base
	belongs_to :user


	has_attached_file :shopphoto, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	has_attached_file :shopphoto1, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	has_attached_file :shopphoto2, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	has_attached_file :shopphoto3, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	has_attached_file :unicproimg1, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	has_attached_file :unicproimg2, styles: { large: "600x600",  medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :shopphoto, content_type: /\Aimage\/.*\z/
	validates_attachment_content_type :shopphoto1, content_type: /\Aimage\/.*\z/
	validates_attachment_content_type :shopphoto2, content_type: /\Aimage\/.*\z/
	validates_attachment_content_type :shopphoto3, content_type: /\Aimage\/.*\z/
	validates_attachment_content_type :unicproimg1, content_type: /\Aimage\/.*\z/
	validates_attachment_content_type :unicproimg2, content_type: /\Aimage\/.*\z/
end
