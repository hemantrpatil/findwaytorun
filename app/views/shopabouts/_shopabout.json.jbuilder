json.extract! shopabout, :id, :shopname, :aboutinfo, :shopmobno, :shopownername, :unicproductsforsale, :created_at, :updated_at
json.url shopabout_url(shopabout, format: :json)
