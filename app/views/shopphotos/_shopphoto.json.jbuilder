json.extract! shopphoto, :id, :img_name, :created_at, :updated_at
json.url shopphoto_url(shopphoto, format: :json)
