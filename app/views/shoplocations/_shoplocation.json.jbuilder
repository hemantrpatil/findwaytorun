json.extract! shoplocation, :id, :location, :created_at, :updated_at
json.url shoplocation_url(shoplocation, format: :json)
