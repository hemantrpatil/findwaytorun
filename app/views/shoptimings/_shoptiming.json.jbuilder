json.extract! shoptiming, :id, :sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :created_at, :updated_at
json.url shoptiming_url(shoptiming, format: :json)
