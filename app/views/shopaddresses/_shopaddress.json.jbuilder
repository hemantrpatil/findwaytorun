json.extract! shopaddress, :id, :name, :streetaddress, :landmark, :city, :state, :pincode, :mobileno, :landlineno, :emailid, :websiteurl, :created_at, :updated_at
json.url shopaddress_url(shopaddress, format: :json)
