require 'test_helper'

class ShopaddressesControllerTest < ActionController::TestCase
  setup do
    @shopaddress = shopaddresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shopaddresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shopaddress" do
    assert_difference('Shopaddress.count') do
      post :create, shopaddress: { city: @shopaddress.city, emailid: @shopaddress.emailid, landlineno: @shopaddress.landlineno, landmark: @shopaddress.landmark, mobileno: @shopaddress.mobileno, name: @shopaddress.name, pincode: @shopaddress.pincode, state: @shopaddress.state, streetaddress: @shopaddress.streetaddress, websiteurl: @shopaddress.websiteurl }
    end

    assert_redirected_to shopaddress_path(assigns(:shopaddress))
  end

  test "should show shopaddress" do
    get :show, id: @shopaddress
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shopaddress
    assert_response :success
  end

  test "should update shopaddress" do
    patch :update, id: @shopaddress, shopaddress: { city: @shopaddress.city, emailid: @shopaddress.emailid, landlineno: @shopaddress.landlineno, landmark: @shopaddress.landmark, mobileno: @shopaddress.mobileno, name: @shopaddress.name, pincode: @shopaddress.pincode, state: @shopaddress.state, streetaddress: @shopaddress.streetaddress, websiteurl: @shopaddress.websiteurl }
    assert_redirected_to shopaddress_path(assigns(:shopaddress))
  end

  test "should destroy shopaddress" do
    assert_difference('Shopaddress.count', -1) do
      delete :destroy, id: @shopaddress
    end

    assert_redirected_to shopaddresses_path
  end
end
