require 'test_helper'

class ShopphotosControllerTest < ActionController::TestCase
  setup do
    @shopphoto = shopphotos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shopphotos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shopphoto" do
    assert_difference('Shopphoto.count') do
      post :create, shopphoto: { img_name: @shopphoto.img_name }
    end

    assert_redirected_to shopphoto_path(assigns(:shopphoto))
  end

  test "should show shopphoto" do
    get :show, id: @shopphoto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shopphoto
    assert_response :success
  end

  test "should update shopphoto" do
    patch :update, id: @shopphoto, shopphoto: { img_name: @shopphoto.img_name }
    assert_redirected_to shopphoto_path(assigns(:shopphoto))
  end

  test "should destroy shopphoto" do
    assert_difference('Shopphoto.count', -1) do
      delete :destroy, id: @shopphoto
    end

    assert_redirected_to shopphotos_path
  end
end
