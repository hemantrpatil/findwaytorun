require 'test_helper'

class ShoptimingsControllerTest < ActionController::TestCase
  setup do
    @shoptiming = shoptimings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shoptimings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shoptiming" do
    assert_difference('Shoptiming.count') do
      post :create, shoptiming: { friday: @shoptiming.friday, monday: @shoptiming.monday, saturday: @shoptiming.saturday, sunday: @shoptiming.sunday, thursday: @shoptiming.thursday, tuesday: @shoptiming.tuesday, wednesday: @shoptiming.wednesday }
    end

    assert_redirected_to shoptiming_path(assigns(:shoptiming))
  end

  test "should show shoptiming" do
    get :show, id: @shoptiming
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shoptiming
    assert_response :success
  end

  test "should update shoptiming" do
    patch :update, id: @shoptiming, shoptiming: { friday: @shoptiming.friday, monday: @shoptiming.monday, saturday: @shoptiming.saturday, sunday: @shoptiming.sunday, thursday: @shoptiming.thursday, tuesday: @shoptiming.tuesday, wednesday: @shoptiming.wednesday }
    assert_redirected_to shoptiming_path(assigns(:shoptiming))
  end

  test "should destroy shoptiming" do
    assert_difference('Shoptiming.count', -1) do
      delete :destroy, id: @shoptiming
    end

    assert_redirected_to shoptimings_path
  end
end
