require 'test_helper'

class ShoplocationsControllerTest < ActionController::TestCase
  setup do
    @shoplocation = shoplocations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shoplocations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shoplocation" do
    assert_difference('Shoplocation.count') do
      post :create, shoplocation: { location: @shoplocation.location }
    end

    assert_redirected_to shoplocation_path(assigns(:shoplocation))
  end

  test "should show shoplocation" do
    get :show, id: @shoplocation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shoplocation
    assert_response :success
  end

  test "should update shoplocation" do
    patch :update, id: @shoplocation, shoplocation: { location: @shoplocation.location }
    assert_redirected_to shoplocation_path(assigns(:shoplocation))
  end

  test "should destroy shoplocation" do
    assert_difference('Shoplocation.count', -1) do
      delete :destroy, id: @shoplocation
    end

    assert_redirected_to shoplocations_path
  end
end
