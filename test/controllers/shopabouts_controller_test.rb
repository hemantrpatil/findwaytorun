require 'test_helper'

class ShopaboutsControllerTest < ActionController::TestCase
  setup do
    @shopabout = shopabouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shopabouts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shopabout" do
    assert_difference('Shopabout.count') do
      post :create, shopabout: { aboutinfo: @shopabout.aboutinfo, shopmobno: @shopabout.shopmobno, shopname: @shopabout.shopname, shopownername: @shopabout.shopownername, unicproductsforsale: @shopabout.unicproductsforsale }
    end

    assert_redirected_to shopabout_path(assigns(:shopabout))
  end

  test "should show shopabout" do
    get :show, id: @shopabout
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shopabout
    assert_response :success
  end

  test "should update shopabout" do
    patch :update, id: @shopabout, shopabout: { aboutinfo: @shopabout.aboutinfo, shopmobno: @shopabout.shopmobno, shopname: @shopabout.shopname, shopownername: @shopabout.shopownername, unicproductsforsale: @shopabout.unicproductsforsale }
    assert_redirected_to shopabout_path(assigns(:shopabout))
  end

  test "should destroy shopabout" do
    assert_difference('Shopabout.count', -1) do
      delete :destroy, id: @shopabout
    end

    assert_redirected_to shopabouts_path
  end
end
